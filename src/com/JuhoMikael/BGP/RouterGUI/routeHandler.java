package com.JuhoMikael.BGP.RouterGUI;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

/**
*
* @author Juho
*/

//A class for handling paths between routers. Heart of routing table management.

public class routeHandler {
	
	String owner;
	String address;
	String stamp;
	String routingPath;
	String FinalPath;
	String path1 = "-------------------------------------";
	String path2 = "-------------------------------------";
	String path3 = "-------------------------------------";
	String path4 = "-------------------------------------";
	String path5 = "-------------------------------------";
	String path6 = "-------------------------------------";
	String path7 = "-------------------------------------";
	String path8 = "-------------------------------------";
	String path9 = "-------------------------------------";
	String path10 = "-------------------------------------";
	String target1;
	
	
	//My personal favourite: The argument monster method.
	
	public void update(String requestor, String origin, String id, String message, String route1, String route2, String route3, String route4, String route5, String route6 ,String route7, String route8, String route9, String route10){
		owner = requestor;
		address = origin;
		stamp = id;
		routingPath = message;
		FinalPath= message.substring(4);
		path1 = route1;
		path2 = route2;
		path3 = route3;
		path4 = route4;
		path5 = route5;
		path6 = route6;
		path7 = route7;
		path8 = route8;
		path9 = route9;
		path10 = route10;
		
		
		
		
		
		System.out.println("address for comparison: "+address);
	
		if(address.equals("1")){
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
				
				
			if((FinalPath.length() <= path1.length()) && (FinalPath.length() <= oldRoute.length()) ){
				
				path1 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    				
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    				
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 1 found but not updated");
		}
		
		else if(address.equals("2")){
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path2.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path2 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    				
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    			
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 2 found but not updated");
		}
		
		else if(address.equals("3")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			
			if((FinalPath.length() <= path3.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path3 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    			
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 3 found but not updated");
		}
		
		else if(address.equals("4")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path4.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path4 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    			
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 4 found but not updated");
		}
		
		else if(address.equals("5")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path5.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path5 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    			
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 5 found but not updated");
		}
		
		else if(address.equals("6")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path6.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path6 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    				
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 6 found but not updated");
		}
		
		
		else if(address.equals("7")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path7.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path7 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    				
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 7 found but not updated");
		}
		
		
		else if(address.equals("8")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path8.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path8 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    				
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    				
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 8 found but not updated");
		}
		
		
		else if(address.equals("9")){
			
			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			if((FinalPath.length() <= path9.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path9 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ address + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ address + ".txt", true)))){
					out2.println(address + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    			
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 9 found but not updated");
		}
		
		else if(address.equals("A")){
			

			routingTableReader check = new routingTableReader();
			check.checkAndReadTable(owner, address);
			String oldRoute = check.getRoute();
			
			
			if((FinalPath.length() <= path10.length()) && (FinalPath.length() <= oldRoute.length())){
				
				path10 = FinalPath;
				
				PrintWriter writer;
    			try {
    				writer = new PrintWriter( owner + "route"+ "10" + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    			
    				e1.printStackTrace();
    			}
    			
    			try(PrintWriter out2 = new PrintWriter(new BufferedWriter(new FileWriter(owner + "route"+ "10" + ".txt", true)))){
					out2.println("10" + ". " + stamp + FinalPath );
    			} catch (IOException e1) {
    				
    				e1.printStackTrace();
    			}
			}
			else
				System.out.println("path data to router 10 found but not updated");
		}
		
		else{
			System.out.println("!!!PathWriter ERROR!!!");
			System.out.println("!!!PathWriter ERROR!!!");
			System.out.println("!!!PathWriter ERROR!!!");
		}
	}
	
	public String getPath(String target){
		target1 = target;
		String target2 = "ok";
		switch (target1){
			case "1": target2 = path1;
					break;
			case "2": target2 = path2;
					break;
			case "3": target2 = path3;
					break;
			case "4": target2 = path4;
					break;
			case "5": target2 = path5;
					break;
			case "6": target2 = path6;
					break;
			case "7": target2 = path7;
					break;
			case "8": target2 = path8;
					break;
			case "9": target2 = path9;
					break;
			case "A": target2 = path10;
					break;
		}
		System.out.println("Target2 on: "+target2);
		return target2;
	}
	
	public String updateRoute1(){
		return path1;
	}
	
	public String updateRoute2(){
		return path2;
	}
	
	public String updateRoute3(){
		return path3;
	}
	
	public String updateRoute4(){
		return path4;
	}
	
	public String updateRoute5(){
		return path5;
	}
	
	public String updateRoute6(){
		return path6;
	}
	
	public String updateRoute7(){
		return path7;
	}
	
	public String updateRoute8(){
		return path8;
	}
	
	public String updateRoute9(){
		return path9;
	}
	
	public String updateRoute10(){
		return path10;
	}
	
	
}
