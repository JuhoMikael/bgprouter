package com.JuhoMikael.BGP.RouterGUI;
/**
*
* @author Juho
*/

// A class to act as an input line handler for the server sockets.


public class serverInputLineHandler {

private String input;




public String inputLineCheck(String inputLine, String AS){
	
	String message = inputLine.substring(4);
	
	 if(inputLine.equals("Bye.")){
			setInput("-----");
			
			
	}
	else if ((inputLine.charAt(1) == '!') && (message.contains(AS))){
			setInput("-----");
			
	}
	else {
		setInput(inputLine);
		
	}
	
		return input;
}


public String getInput() {
	return input;
}


public void setInput(String input) {
	this.input = input;
}





}