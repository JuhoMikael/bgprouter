package com.JuhoMikael.BGP.RouterGUI;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Juho
 */

// Class to set up and manage the fully customizable router port 3

 class routerServerPort3 implements Runnable  {
	   boolean reachable = false;
	     boolean reachable2 = false;
	     private final Thread t;
	    private volatile boolean shouldStop = false;
	    String osoite1;
	    String osoite2;
	    int osoite;
	    boolean flag = false;
	    char source;
	    String message;
	    String sanoma;
	    String sanomaUlos = "oikeaPerse";
	    String outPort="-";
	    private static final String id = "D";
	    
	    routeHandler handlaaja = new routeHandler();
	    String path1 = "------------------------------";   
	    String path2 = "------------------------------";
	    String path3 = "------------------------------";
	    String path4 = "------------------------------";
	    String path5 = "------------------------------";
	    String path6 = "------------------------------";
	    String path7 = "------------------------------";
	    String path8 = "------------------------------";
	    String path9 = "------------------------------";
	    String path10 = "------------------------------";
	    
	    
	   
	    
	    public String getSanoma(){
	    	return sanoma;
	    }
	    
	    public boolean getFlags(){
	    	return flag;
	    }
	    
	    public char getSource(){
	    	return source;
	    }
	    
	    public String getMessage(){
	    	return message;
	    }
	    
	    public void setSanoma(String sanoma3){
	    	sanomaUlos = sanoma3;
	    }
	    
    
    public routerServerPort3() {
	t = new Thread(this, "Ping Thread");
       
    }
    
    public void start(String port, String router) {
    	osoite1 = port;
    	osoite2 = router;
    	osoite = Integer.parseInt(osoite1);
    	
    	
    	
        t.start();
    }

    public void stop() { 
    	
    	t.interrupt();
         shouldStop = true;
         
    }
 
    public void run()  {
	 
    	ServerSocket serverSocket = null; 

        try { 
             serverSocket = new ServerSocket(osoite); 
            } 
        catch (IOException e) 
            { 
             System.err.printf("Could not listen on port: %d." , osoite); 
             
            } 
    	
        while(true){
        	
           

           Socket clientSocket = null; 
           System.out.printf ("Odotetaan yhteytt� porttiin: %d.....", osoite);
           reachable = true;

           try { 
                clientSocket = serverSocket.accept(); 
               } 
           catch (IOException e) 
               { 
                System.err.println("Accept failed."); 
                System.exit(1); 
               } 
           
           System.out.println ("Connection successful");
           System.out.println ("Waiting for input.....");
           reachable2 = true;
           PrintWriter out;
           BufferedReader in;
           String inputLine; 
           String prevInputLine="tyhja";
           int count = 0;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), 
			                                     true);
			in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream()));
			 

	           while ((inputLine = in.readLine()) != null) 

               { 
	        	   
	        	   
	        	serverInputLineHandler inputHandler = new serverInputLineHandler();
	        	routingTableReader routeReader = new routingTableReader();
        	  
                out.println("ack"); 
                System.out.println("resenperse");
                sanoma = inputHandler.inputLineCheck(inputLine, osoite2);
                
                
                System.out.println("FromBELOW: "+inputLine);
              
                source=inputLine.charAt(0);
               
                if (inputLine.equals(prevInputLine)){
                	count++;
                	if(count == 5){
                	sanoma = "-----";
                	count = 0;
                	}
                	else
                		sanoma = sanoma;
                }
                
                else if((inputLine.charAt(1) != '!') && (inputLine.charAt(3) == 'm')){
                	flag = true;
                	String o = Character.toString(inputLine.charAt(1));
                	routeReader.checkAndReadTable(osoite1, o);
                	outPort = Character.toString(routeReader.getRoute().charAt(0));
                	message = "Message received from " + Character.toString(inputLine.charAt(1)) + ": "+ inputLine.substring(4);
                	
                }
                
                else if(inputLine.charAt(2) == '0')
                	out.println("-----");
                
                else if((inputLine.charAt(1) == osoite2.charAt(0)) && (inputLine.charAt(2)=='d')){
                flag = true;
           	out.println("192.168.1.69");
           	System.out.println("iposoite");
           	message = "DHCP address sent: 192.168.11.69";
           	
                }
                
                else if((inputLine.charAt(1) == osoite2.charAt(0)) && (inputLine.charAt(2)=='p')){
	                flag = true;
                	out.println("CRP1");
                	message = "Client Ping Received";
                	
	                }
                
                else if (inputLine.charAt(1) == osoite2.charAt(0)){
                	flag = true;
                	
                }
                
                else if (inputLine.charAt(1) == '!'){
                	flag=true;
                	message = "Routing info received from " + Character.toString(inputLine.charAt(0)) + ": "+ Character.toString(inputLine.charAt(0)) + ". " + inputLine.substring(4);
                	handlaaja.update(osoite2, Character.toString(inputLine.charAt(0)), id, inputLine, path1, path2, path3, path4, path5, path6, path7, path8, path9, path10);
                	/*path1 = handlaaja.updateRoute1();
                	path2 = handlaaja.updateRoute2();
                	path3 = handlaaja.updateRoute3();
                	path4 = handlaaja.updateRoute4();
                	path5 = handlaaja.updateRoute5();
                	path6 = handlaaja.updateRoute6();
                	path7 = handlaaja.updateRoute7();
                	path8 = handlaaja.updateRoute8();
                	path9 = handlaaja.updateRoute9();
                	path10 = handlaaja.updateRoute10();
                	*/

                }
             
                else if (inputLine.equals("Bye.")) {
                	message = "Connection closed";
                    break; }
                
                else if (inputLine.charAt(2) == 'd'){
                	out.println("192.168.1.69");
                	message = "DHCP address sent: 192.168.11.69";
                }
                else if (inputLine.charAt(2) == 'p'){
                	out.println("RCP1");
                	message = "Host Ping Reply";
                	System.out.println("RCP1");
                }
                else {
                	flag = false;
                }
                prevInputLine = inputLine;
               } 
	           reachable = false;
	           reachable2 = false;
	           flag=false;
	           out.close(); 
	           
				in.close();
				clientSocket.close(); 
		           serverSocket.close(); 
	           
		} catch (IOException e1) {
		
			e1.printStackTrace();
			 reachable = false;
	           reachable2 = false;
	           flag=false;
		} 
          

          } 
        }
   
	}
    
 
    
