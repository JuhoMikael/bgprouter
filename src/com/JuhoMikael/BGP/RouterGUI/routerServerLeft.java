package com.JuhoMikael.BGP.RouterGUI;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Juho
 */

// Class for creating and managing the Router port 1 server

 class routerServerLeft implements Runnable  {
	 
	 
     boolean reachable = false;
     boolean reachable2 = false;
     private final Thread t;
    private volatile boolean shouldStop = false;
    String osoite1;
    int osoite;
    String sanoma ="hello";
    boolean flag = false;
    char source;
    String message;
    String sanomaUlos = "vasenulooos";
    String outPort = "";
    private static final  String id = "L";
    
    routeHandler handlaaja = new routeHandler();
    
    //Rather unnecessary set of variables but might come handy in future development
    String path1 = "------------------------------";   
    String path2 = "------------------------------";
    String path3 = "------------------------------";
    String path4 = "------------------------------";
    String path5 = "------------------------------";
    String path6 = "------------------------------";
    String path7 = "------------------------------";
    String path8 = "------------------------------";
    String path9 = "------------------------------";
    String path10 = "------------------------------";
    
    
    public routerServerLeft() {
	t = new Thread(this, "Ping Thread");
       
    }
    
    public String getSanoma(){
    	return sanoma;
    }
    
    public boolean getFlags(){
    	return flag;
    }
    
    public char getSource(){
    	return source;
    }
    public String getMessage(){
    	return message;
    }
    
    public void setSanoma(String sanoma1){
    	sanomaUlos = sanoma1;
    }
    
    public void start(String port) {
    	osoite1 = port;
    	osoite = Integer.parseInt(osoite1);
    	
    	if(osoite == 1) {
	  		osoite = 45101;
	  	}
	  	
	  	else if (osoite == 2) {
	  		osoite = 45201;
	  	}
	  	
	  	else if(osoite == 3) {
	  		osoite = 45301;
	  	}
	  	
	  	else if(osoite == 4) {
	  		osoite = 45401;
	  	}
    	
	  	else if(osoite == 5) {
	  		osoite = 45501;
	  	}
    	
	  	else if(osoite == 6) {
	  		osoite = 45601;
	  	}
    	
	  	else if(osoite == 7) {
	  		osoite = 45701;
	  	}
    	
	  	else if(osoite == 8) {
	  		osoite = 45801;
	  	}
    	
	  	else if(osoite == 9) {
	  		osoite = 45901;
	  	}
	  	
	  	else
	  			osoite = 46001;
	  	
    	
        t.start();
    }

    public void stop() {   
         shouldStop = true;
         
    }
 
    public void run()  {
	 
    	
    	ServerSocket serverSocket = null; 

        try { 
             serverSocket = new ServerSocket(osoite); 
            } 
        catch (IOException e) 
            { 
             System.err.printf("Could not listen on port: %d." , osoite); 
             System.exit(1); 
            } 
        
        while(true){
        	
           

           Socket clientSocket = null; 
           System.out.printf ("Odotetaan yhteytt� porttiin: %d.....", osoite);
           reachable = true;

           try { 
                clientSocket = serverSocket.accept(); 
               } 
           catch (IOException e) 
               { 
                System.err.println("Accept failed."); 
                
               } 
           
           System.out.println ("Connection successful");
           System.out.println ("Waiting for input.....");
           reachable2 = true;
           PrintWriter out;
           BufferedReader in;
           String inputLine; 
           String prevInputLine="tyhja";
           int count = 0;
		try {
			out = new PrintWriter(clientSocket.getOutputStream(), 
			                                     true);
			in = new BufferedReader(new InputStreamReader( clientSocket.getInputStream()));
			 
			int i =1;
			while ( i ==1 ){
	           while ((inputLine = in.readLine()) != null) 
	               { 
	        	   serverInputLineHandler inputHandler = new serverInputLineHandler();
	        	   routingTableReader routeReader = new routingTableReader();
	        	   
	                out.println("ack"); 
	                System.out.println("resendvasen");
	                sanoma = inputHandler.inputLineCheck(inputLine, osoite1);
	                
	                System.out.println("FromLEFT: "+inputLine);
	  
	                source=inputLine.charAt(0);
	               
	                if (inputLine.equals(prevInputLine)){
	                	count++;
	                	if(count == 5){
	                	sanoma = "-----";
	                	count = 0;
	                	}
	                	else
	                		sanoma = sanoma;
	                }
	                
	                else if((inputLine.charAt(1) != '!') && (inputLine.charAt(3) == 'm')){
	                	flag = true;
	                	String o = Character.toString(inputLine.charAt(1));
	                	routeReader.checkAndReadTable(osoite1, o);
	                	outPort = Character.toString(routeReader.getRoute().charAt(0));
	                	System.out.println("Outport from server Left: "+outPort);
	                	message = "Message received from " + Character.toString(inputLine.charAt(1)) + ": "+ inputLine.substring(4);
	                	
	                }
	                
	                else if(inputLine.charAt(2) == '0')
	                	out.println("-----");
	                
	                else if((inputLine.charAt(1) == osoite1.charAt(0)) && (inputLine.charAt(2)=='d')){
	                flag = true;
                	out.println("192.168.1.69");
                	System.out.println("iposoite");
                	message = "DHCP address sent: 192.168.11.69";
                	
	                }
	                
	                else if((inputLine.charAt(1) == osoite1.charAt(0)) && (inputLine.charAt(2)=='p')){
		                flag = true;
	                	out.println("CRP1");
	                	message = "Client Ping Received";
	                	
		                }
	                
	                else if (inputLine.charAt(1) == osoite1.charAt(0)){
	                	flag = true;
	                	
	                }
	                
	                else if (inputLine.charAt(1) == '!'){
	                	flag=true;
	                	message = "Routing info received from " + Character.toString(inputLine.charAt(0)) + ": "+ Character.toString(inputLine.charAt(0)) + ". " + inputLine.substring(4);
	                	
	                	handlaaja.update(osoite1, Character.toString(inputLine.charAt(0)), id, inputLine, path1, path2, path3, path4, path5, path6, path7, path8, path9, path10);
	                	//Waiting for future applicational value ...
	                	/*path1 = handlaaja.updateRoute1();
	                	path2 = handlaaja.updateRoute2();
	                	path3 = handlaaja.updateRoute3();
	                	path4 = handlaaja.updateRoute4();
	                	path5 = handlaaja.updateRoute5();
	                	path6 = handlaaja.updateRoute6();
	                	path7 = handlaaja.updateRoute7();
	                	path8 = handlaaja.updateRoute8();
	                	path9 = handlaaja.updateRoute9();
	                	path10 = handlaaja.updateRoute10();
	                	System.out.println("Route1: "+path1);
	                	System.out.println("Route2: "+path2);
	                	System.out.println("Route3: "+path3);
	                	System.out.println("Route4: "+path4);
	                	System.out.println("Route5: "+path5);
	                	System.out.println("Route6: "+path6);
	                	System.out.println("Route7: "+path7);
	                	System.out.println("Route8: "+path8);
	                	System.out.println("Route9: "+path9);
	                	System.out.println("Route10: "+path10);*/
	                	
	                	
	                }
	               
	                else if (inputLine.equals("Bye.")) {
	                	message = "Connection closed";
	                
	     	           reachable2 = false;
	     	           out.close(); 
	     	           
	     				in.close();
	     				clientSocket.close(); 
	                     }
	                
	                else if ((inputLine.charAt(2) == 'd') && (inputLine.charAt(1) == osoite1.charAt(0))){
	                	out.println("192.168.1.69");
	                	message = "DHCP address sent: 192.168.11.69";
	                }
	                else if ((inputLine.charAt(2) == 'p') && (inputLine.charAt(1) == osoite1.charAt(0))){
	                	out.println("RCP1");
	                	message = "Host Ping Reply";
	                	System.out.println("RCP1");
	                }
	                else {
	                	flag = false;
	                }
	                prevInputLine = inputLine;
	               } 
	           System.out.println("Sammuu");}
	           reachable = false;
	           reachable2 = false;
	           flag=false;
	           out.close(); 
	           
				in.close();
				clientSocket.close(); 
		          serverSocket.close(); 
	           
		} catch (IOException e1) {
			
			e1.printStackTrace();
			reachable = false;
	           reachable2 = false;
	           flag=false;
		} 
          

          } 
        }
   
	}
    
 
    
