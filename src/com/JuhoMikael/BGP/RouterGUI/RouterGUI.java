package com.JuhoMikael.BGP.RouterGUI;
 import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Calendar;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JToggleButton;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
*
* @author Juho
*/


public class RouterGUI extends JFrame{
	
	//Declaring Layouts
	FlowLayout experimentLayout = new FlowLayout();
	GridLayout myLayout = new GridLayout(4,3);
	
	// Declaring instances of sub-class threads
    SimpleThread oujee = new SimpleThread();
    SimpleThread2 oujee2 = new SimpleThread2();

    
    // Declaring instances of server sockets and client sockets
	routerServerLeft svasen = new routerServerLeft();
	routerServerRight soikea = new routerServerRight();
	routerClientLeft cvasen = new routerClientLeft();
	routerClientRight coikea = new routerClientRight();
	routerServerPort3 configserver = new routerServerPort3();
	routerClientPort3 configclient = new routerClientPort3();
	
   
    // More GUI stuff
     JLabel nimi = new JLabel("RouterGUI");
     JToggleButton button2 = new JToggleButton("K�ynnist�");
     JButton button3 = new JButton("ClientAttack");
     JButton trace = new JButton("Trace");
     JButton routingTable = new JButton("Path");
     JLabel tila1 = new JLabel("");
     JLabel ASID = new JLabel("");
     JToggleButton discover = new JToggleButton("Advertize Routes");
     
     JLabel label = new JLabel("Select Router Number:");
     
     String[] routerNumbers = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
     JComboBox<String> router = new JComboBox<>(routerNumbers);
     
     String[] routeSelect = new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
     JComboBox<String> route = new JComboBox<>(routeSelect);
     
     
     
     JLabel empty1 = new JLabel("");
     JLabel empty2 = new JLabel("");
     JLabel portAddress = new JLabel("Port3 Configure ServerPort:");
     JTextField portAddress1 = new JTextField("");
     JButton portAddressSend = new JButton("Create");
     JLabel portAddress2 = new JLabel("Port3 Configure ClientPort");
     JTextField portAddress21 = new JTextField("");
     JButton portAddressSend2 = new JButton("Create");
     JLabel info1 = new JLabel("");
     JLabel info2 = new JLabel("Port1                  Port2");
     JLabel info3 = new JLabel("Port3");
     
     
     JLabel port1 = new JLabel();
     JLabel port2 = new JLabel();
     JLabel port3 = new JLabel();
     
     JLabel target = new JLabel();
     JLabel receive = new JLabel();
    
    Boolean blaa = true;
    int i = 0;
    int j = 0;
    int b = 0;
    int osoitesvasen;
    public boolean[] array = new boolean[3];
    
    
    
    
  
    public RouterGUI(String name) {
        super(name);
       
    }
     
    //Placing components onto panel
    public void addComponentsToPane(final Container pane) {
        final JPanel compsToExperiment = new JPanel();
        compsToExperiment.setLayout(experimentLayout);
        experimentLayout.setAlignment(FlowLayout.TRAILING);
        JPanel controls = new JPanel();
        controls.setLayout(myLayout);
        
        JPanel PortStatus = new JPanel();
        PortStatus.setLayout(new FlowLayout());
        nimi.setFont(new Font("Arial", Font.BOLD, 21));
        ASID.setFont(new Font("Arial", Font.BOLD, 21));
        
        
        
        compsToExperiment.add(nimi);
        compsToExperiment.add(label);
        compsToExperiment.add(router);
        compsToExperiment.add(button2);
        compsToExperiment.add(button3);
        compsToExperiment.add(trace);
        compsToExperiment.add(route);
        compsToExperiment.add(routingTable);

       
        
        compsToExperiment.setComponentOrientation(
                ComponentOrientation.LEFT_TO_RIGHT);
         
      
        controls.add(empty1);
        controls.add(ASID);
        controls.add(discover);
        controls.add(portAddress);
        controls.add(portAddress1);
        controls.add(portAddressSend);
        controls.add(portAddress2);
        controls.add(portAddress21);
        controls.add(portAddressSend2);
        controls.add(info1);
        controls.add(info2);
        controls.add(info3);
        
        controls.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        
        PortStatus.add(port1);
        PortStatus.add(port2);
        PortStatus.add(port3);
        PortStatus.add(target);
        PortStatus.add(receive);
        
        
        
        //Adding action listeners for all the buttons and assigning functionality 
        
        portAddressSend.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	String a;
            	a = portAddress1.getText();
            	configserver.start(a, (String)router.getSelectedItem());
            	info3.setText("Port3 Server@" + portAddress1.getText());
            
        }});
        
        portAddressSend2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
            	String a;
            	a = portAddress21.getText();
            	configclient.start(a, (String)router.getSelectedItem());
            	info3.setText(info3.getText() + " & Client@" + portAddress21.getText());
            
        }});
        
       button2.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
    
            	if(button2.isSelected()){
            	 
            	ASID.setText("AS" + (String)router.getSelectedItem());
            	//valvonta.start((String)router.getSelectedItem());
            	button2.setText("K�ynniss�");
            	svasen.start((String)router.getSelectedItem());
            	soikea.start((String)router.getSelectedItem());
            	
            	oujee.start();
            	oujee2.start();
            	
            	PrintWriter writer;
    			try {
    				writer = new PrintWriter("logRouter"+ ((String)router.getSelectedItem()) + ".txt");
    				writer.print("");
    	        	writer.close();
    			} catch (FileNotFoundException e1) {
    				e1.printStackTrace();
    			}
            	
            		}
            		else{
            		
            			oujee2.setJ(1);
            			cvasen.setViesti("Bye.");
            			coikea.setViesti("Bye.");
            			configclient.setViesti("Bye.");
            			System.out.println("Viesti:"+coikea.viesti);
            			button2.setText("�L�k�ynnist�");
            			button2.setVisible(false);
            			
            		}
            		}
        	   

        }); 
        
         button3.addActionListener(new ActionListener(){
        public void actionPerformed(ActionEvent e){
        	cvasen.start((String)router.getSelectedItem());
        	coikea.start((String)router.getSelectedItem());
        	button3.setVisible(false);

        }}); 
         
         trace.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e){
            	 
                 JTextArea textArea = new JTextArea(15, 70);
                 try {
                	   FileReader fr = new FileReader("logRouter" + ((String)router.getSelectedItem()) + ".txt");
                	   BufferedReader reader = new BufferedReader(fr);
                	   String textLine;

                	   while((textLine=reader.readLine())!=null){
                	     textLine = reader.readLine();
                	     textArea.read(reader,"textArea");
                	   } 
                	}
                	   catch (IOException ioe) {
                	   System.err.println(ioe);
                	}             
                 textArea.setEditable(false);
                  
                 JScrollPane scrollPane = new JScrollPane(textArea);
               
                 JOptionPane.showMessageDialog(compsToExperiment, scrollPane);
                 receive.setVisible(false);
         }}); 
        
         
         routingTable.addActionListener(new ActionListener(){
             
			public void actionPerformed(ActionEvent e){
                 JTextArea textArea = new JTextArea(15, 70);
                 try {
              	   FileReader fr = new FileReader(((String)router.getSelectedItem())+ "route" + ((String)route.getSelectedItem()) + ".txt");
              	   BufferedReader reader = new BufferedReader(fr);
              	   String textLine2;

              	   while((textLine2=reader.readLine())!=null){
              	     textArea.append(textLine2 + "\n");
              	   } 
              	}
              	   catch (IOException ioe) {
              	   System.err.println(ioe);
              	}             
               textArea.setEditable(false);
               JScrollPane scrollPane = new JScrollPane(textArea);
                
               JOptionPane.showMessageDialog(compsToExperiment, scrollPane);
           
       }});  
        
         
         discover.addActionListener(new ActionListener(){
             
			public void actionPerformed(ActionEvent e){

				if(discover.isSelected()==true){
					System.out.println("sammutus...." + oujee2.getState().toString());
					oujee2.setI(1);
				
                 coikea.setViesti((String)router.getSelectedItem() +"!9b");
                 cvasen.setViesti((String)router.getSelectedItem() +"!9b");
                 configclient.setViesti((String)router.getSelectedItem() + "!9b");
                 
                 System.out.println("message set to broadcast!!!!: " +((String)router.getSelectedItem() +"!bc"));
				}
				else {
					oujee2.setI(0);
					coikea.setViesti("-----");
					cvasen.setViesti("-----");
					configclient.setViesti("-----");
					System.out.println("Taas k�ynniss�" + oujee2.getState().toString());
				}}});  
        
        
        pane.add(compsToExperiment, BorderLayout.NORTH);
        pane.add(controls, BorderLayout.CENTER);
        
        pane.add(PortStatus, BorderLayout.SOUTH);
    }
     
    
    
    private static void createAndShowGUI() {
        
        RouterGUI frame = new RouterGUI("VRM - Virtual Router Monitor");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       
        frame.addComponentsToPane(frame.getContentPane());
        frame.setSize(700,300);
        frame.setVisible(true);
    }
    
    
     
    public static void main(String[] args) throws IOException {
    	
    	
        try {
           
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (UnsupportedLookAndFeelException ex) {
            ex.printStackTrace();
        } catch (IllegalAccessException ex) {
            ex.printStackTrace();
        } catch (InstantiationException ex) {
            ex.printStackTrace();
        } catch (ClassNotFoundException ex) {
            ex.printStackTrace();
        }
       
        
        
        UIManager.put("swing.boldMetal", Boolean.FALSE);
       
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
                
            }
        });
        
        
    }
    
    // First Sub-class for monitoring port statuses
    
    class SimpleThread extends Thread {
        public SimpleThread() {
        	
    	
    	
        }
        
        public  void compare1(){
	    		
        		if(svasen.reachable == true && svasen.reachable2 == true)  {
	    		 port1.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45on.png")));
        		}
        		else if (svasen.reachable == true){
        			port1.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45semi.png")));
        		}
        		else
        			port1.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45off.png")));
	     }
        
        public  void compare2(){
        	
        	if(soikea.reachable == true && soikea.reachable2 == true)  {
	    		 port2.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45on.png")));
       		}
       		else if (soikea.reachable == true){
       			port2.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45semi.png")));
       		}
       		else
       			port2.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45off.png")));
	    		 
	     }
        
        public  void compare3(){
        	
        	if(configserver.reachable == true && configserver.reachable2 == true)  {
	    		 port3.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45on.png")));
      		}
      		else if (configserver.reachable == true){
      			port3.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45semi.png")));
      		}
      		else
      			port3.setIcon(new javax.swing.ImageIcon(getClass().getResource("rj45off.png")));
	    		 
	     }
        
        public void portStatus(){
        	array[0] = svasen.reachable2;
        	array[1] = soikea.reachable2;
        	array[2] = configserver.reachable2;
        	System.out.print(array[0]);
        	System.out.print(array[1]);
        	System.out.print(array[2]);
        	System.out.println("");
        }
        
       
        
        public void run() {
    	while (i == 0) {
    	     
    		compare1();
    		compare2();
    		compare3();
    		
    		 portStatus();
    	     
                try {
    		sleep(1000);
                   
    	    } catch (InterruptedException e) {}
    	}
    	
        }
    }
    
    
    // Second subclass for message and route handling
    class SimpleThread2 extends Thread {
        public SimpleThread2() {
        	
        }
        
        public void setI(int a){
        	i =a;
        }
        
        public void setJ(int p){
        	j =p;
        }
        
        public void watchDog(){
        	
        	
        	//not yet bug fixed functionality
        	/*messageCentral mc = new messageCentral();
        	String sanoma = mc.checkValid(svasen.getSanoma(), array[0]);
        	String sanoma2 = mc.checkValid(soikea.getSanoma(), array[1]);
        	String sanoma3 = mc.checkValid(configserver.getSanoma(), array[2]);
        	*/
        	
        	String sanoma = svasen.getSanoma();
        	String sanoma2 = soikea.getSanoma();
        	String sanoma3 = configserver.getSanoma();
        	String sanomaCheck = "-";
        	String sanoma2Check = "-";
        	String sanoma3Check = "-";
        	
        	System.out.println((String)router.getSelectedItem()+".router<"+"vasen: "+svasen.flag);
        	System.out.println((String)router.getSelectedItem()+".router<"+"oikea: "+soikea.flag);
        	System.out.println((String)router.getSelectedItem()+".router<"+"ala: "+configserver.flag);
        	messageHandler hand = new messageHandler();
    		
        	
        	if(sanoma.length()>= 100){
        		
        		coikea.setViesti("-----");
        		cvasen.setViesti("-----");
        		configclient.setViesti("-----");
        		receive.setVisible(false);
        	}
        	else if((svasen.flag==true) && (sanoma != "null") && (sanoma != "-----")){
        		sanoma = hand.getMessage(sanoma);
        		if(sanoma == sanomaCheck){
        			sanoma = "-----";
        			receive.setVisible(false);
        		}
        		
        		else if(sanoma.charAt(1) != '!'){
        			String wanted = Character.toString(sanoma.charAt(1));
        			String myself = (String)router.getSelectedItem();
        			String outPort = svasen.outPort;
        			System.out.println("myself: "+myself);
        			System.out.println("wanted: "+wanted);
        			System.out.println("sanoma: " +sanoma);
        			
        			if(wanted.equals(myself)) {
        				receive.setVisible(true);
        				receive.setIcon(new javax.swing.ImageIcon(getClass().getResource("message.png")));
        			}
        			else if((outPort.equals("L")) && array[0] == true){
        				cvasen.setViesti(sanoma);
        				receive.setVisible(false);
        			}
        			else if((outPort.equals("R")) && array[1] == true){
        				coikea.setViesti(sanoma);
        				receive.setVisible(false);
        			}
        			else if((outPort.equals("D")) && array[2] == true){
        				configclient.setViesti(sanoma);
        				receive.setVisible(false);
        			}
        			else {
        				coikea.setViesti(sanoma);
        				configclient.setViesti(sanoma);
        				receive.setVisible(false);
        			}
        		}
        		else{
        		coikea.setViesti(sanoma);
        		System.out.println("sanomaoikea"+ sanoma);
        		configclient.setViesti(sanoma);
        		receive.setVisible(false);
        		}
        		}
        	
        	else if ((soikea.flag == true) && (sanoma2 != "null") && (sanoma != "-----")){
        		sanoma2 = hand.getMessage(sanoma2);
        		if(sanoma2 == sanoma2Check){
        			sanoma2 = "-----";
        		}
        		
        		else if(sanoma2.charAt(1) != '!'){
        			String wanted = Character.toString(sanoma2.charAt(1));
        			String myself = (String)router.getSelectedItem();
        			String outPort = soikea.outPort;
        			System.out.println("myself: "+myself);
        			System.out.println("wanted: "+wanted);
        			System.out.println("sanoma: " +sanoma2);
        			
        			if(wanted.equals(myself)) {
        				receive.setVisible(true);
        				receive.setIcon(new javax.swing.ImageIcon(getClass().getResource("message.png")));
        			}
        			else if((outPort.equals("L")) && array[0] == true){
        				cvasen.setViesti(sanoma2);
        			}
        			else if((outPort.equals("R")) && array[1] == true){
        				coikea.setViesti(sanoma2);
        			}
        			else if((outPort.equals("D")) && array[2] == true){
        				configclient.setViesti(sanoma2);
        			}
        			else {
        				configclient.setViesti(sanoma2);
        				cvasen.setViesti(sanoma2);
        			}
        		}
        		
        		else{
        		cvasen.setViesti(sanoma2);
        	 	configclient.setViesti(sanoma2);
        		}
        		}
        	 	
        	else if ((configserver.flag == true) && (sanoma3 != "null") && (sanoma!="-----")){
        		sanoma3 = hand.getMessage(sanoma3);
        		if(sanoma3 == sanoma3Check){
        			sanoma3 = "-----";
        		}
        		
        		else if(sanoma3.charAt(1) != '!'){
        			String wanted = Character.toString(sanoma3.charAt(1));
        			String myself = (String)router.getSelectedItem();
        			String outPort = configserver.outPort;
        			System.out.println("myself: "+myself);
        			System.out.println("wanted: "+wanted);
        			System.out.println("sanoma: " +sanoma3);
        			
        			if(wanted.equals(myself)) {
        				receive.setVisible(true);
        				receive.setIcon(new javax.swing.ImageIcon(getClass().getResource("message.png")));
        			}
        			else if((outPort.equals("L")) && array[0] == true){
        				cvasen.setViesti(sanoma3);
        			}
        			else if((outPort.equals("R")) && array[1] == true){
        				coikea.setViesti(sanoma3);
        			}
        			else if((outPort.equals("D")) && array[2] == true){
        				configclient.setViesti(sanoma3);
        			}
        			else {
        				coikea.setViesti(sanoma3);
        				cvasen.setViesti(sanoma3);
        			}
        		}
        		
        		else{
        		cvasen.setViesti(sanoma3);
        	 	coikea.setViesti(sanoma3);
        		}
        		}
        	 	
        	 
        	else {
        		cvasen.setViesti("-----");
        		coikea.setViesti("-----");
        		configclient.setViesti("-----");
        	}
        		
        	
        	
        }
        
        public void watchDog2(){
        	
        	if ((svasen.flag == true) || (soikea.flag == true) ||(configserver.flag == true)){
        		target.setVisible(true);
        		target.setIcon(new javax.swing.ImageIcon(getClass().getResource("target.png")));
        		if(svasen.flag == true) {
        		String lSource = Character.toString(svasen.source);
        		java.util.Date date= new java.util.Date();
        		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("logRouter"+ svasen.osoite1 +".txt", true)))) {
        		    out.println("---->" + date + "From:" + lSource + ": " + svasen.getMessage() + "\n");
        		    sleep(500);
        		}catch (IOException | InterruptedException e) {
        		}
        		}
        		else if(soikea.flag == true){
        			
        			System.out.println("oikea saa viestin");
            		String rSource = Character.toString(soikea.source);
            		java.util.Date date= new java.util.Date();
            		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("logRouter"+ soikea.osoite1 +".txt", true)))) {
            		    out.println("<-----" + date + "From:" + rSource + ": " + soikea.getMessage()+"\n");
            		    sleep(500);
            		}catch (IOException | InterruptedException e) {


        		}
        	}
        		
        		else if(configserver.flag == true){
        			
        			System.out.println("alap�� saa viestin");
            		String bSource = Character.toString(configserver.source);
            		java.util.Date date= new java.util.Date();
            		try(PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("logRouter"+ configserver.osoite2 +".txt", true)))) {
            		    out.println("---^---" + date + "From:" + bSource + ": " + soikea.getMessage()+ "\n");
            		    sleep(500);
            		}catch (IOException | InterruptedException e) {

        		}
        	}
        		
        		else 
        			System.out.println("noflag");
        	}
        	else 
        		target.setVisible(false);
        }
        
        public void autoRoute(){
        	
        	Calendar calendar = Calendar.getInstance();
    		int minutes = calendar.get(Calendar.MINUTE);
    		int seconds = calendar.get(Calendar.SECOND);
    		int id = Integer.parseInt((String)router.getSelectedItem());
    		System.out.println(minutes);
    		
    		if((id == 1) && (minutes == 21) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
					
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 2) && (minutes == 22) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 3) && (minutes == 23) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 4) && (minutes == 24) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 5) && (minutes == 25) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 6) && (minutes == 26) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 7) && (minutes == 27) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 8) && (minutes == 28) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		
    		else if((id == 9) && (minutes == 29) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
    		else if((id == 10) && (minutes == 30) && (seconds < 10)){
    			discover.doClick();
    			discover.setSelected(true);
    			
    			
    				try {
						Thread.sleep(30000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
    				finally{
    					discover.doClick();
    	    			discover.setSelected(false);
    				}
    		}
        }
        
        
       
        
        public void run() {
	
        	
        	while (b  == 0) {
    		
    		while(i==0 && j==0){
    		watchDog();
    		watchDog2();
    		autoRoute();
    		System.out.println("py�rii");
    		  try {
    	    		sleep(1500);
    	                   
    	    	    } catch (InterruptedException e) {}
    	    	
    		
    	    } 
    	
    	System.out.println("ei py�ri");
    	try {
			sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
    	}
        }
    }
        }
    
    
    
    
    
   
