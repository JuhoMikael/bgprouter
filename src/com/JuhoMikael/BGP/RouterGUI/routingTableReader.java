package com.JuhoMikael.BGP.RouterGUI;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
*
* @author Juho
*/

// A class to function as an interface for accessing routing table information

public class routingTableReader {
	
 private String route = "-----------------------------------------------------";
 private String oldRouteInfo;

public void checkAndReadTable(String owner, String destination){
	
	
	File f = new File(owner + "route" + destination +".txt");

    if(f.exists()){
        System.out.println("File named: "+f.getName()+", exists " );
        
        try {
      	   FileReader fr = new FileReader(owner + "route" + destination + ".txt");
      	   BufferedReader reader = new BufferedReader(fr);
      	   String route1;
 
      	   while((route1=reader.readLine())!=null){
      	    
      	     System.out.println("route: " + route1);
      	     if(route1 != null)
      	    	 oldRouteInfo=route1;
      	   } 
   
      	   System.out.println(oldRouteInfo);
      	   setRoute(oldRouteInfo.substring(3));
      	}
      	   catch (IOException ioe) {
      	   System.err.println(ioe);
      	}        
    }
    else{
        System.out.println("File not found!");
        
    }
}

public String getRoute() {
	return route;
}

public void setRoute(String route) {
	this.route = route;
}



}
