package com.JuhoMikael.BGP.RouterGUI;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

/**
*
* @author Juho
*/

// A class for creating and running the client socket on the fully configurable router port 3

public class routerClientPort3 implements Runnable {
	boolean reachable = false;
    private final Thread t;
   private volatile boolean shouldStop = false;
   private String routerNumber;
   int routerAddress;
   String RouterAddress1;
   String RouterAddress2;
   String serverHostname = new String ("127.0.0.1");
   public String userInput = "Ping Hello";
   boolean on;
   public String viesti = "-----";
   
   public routerClientPort3() {
	
	
	t = new Thread(this, "Client Thread");
	
      
   }
   
   public void setMessage(String message){
	   userInput = message;
	   
   }
   
   public void setViesti(String message1){
	   viesti = message1;
	   if(viesti.charAt(2)=='!'){
		   viesti = (viesti + "," + RouterAddress1);
	   }
	   
   }
   
   public void start(String osoite, String router) {
       
	  RouterAddress1 = osoite;
	  RouterAddress2 = router;
	  routerAddress = Integer.parseInt(RouterAddress1);
	  
	  
	  	
	   
	   t.start();
       on = true;
   }

   public void stop() {   
        t.interrupt();
        
        on = false;
   }

   public void run() {
	 
	 
       while(on == true){
    	   Socket echoSocket = null;
           PrintWriter out = null;
           BufferedReader in = null;
           int i=0;
           
           try {
          
               echoSocket = new Socket(serverHostname, routerAddress);
               out = new PrintWriter(echoSocket.getOutputStream(), true);
               in = new BufferedReader(new InputStreamReader(
                                           echoSocket.getInputStream()));
               System.out.printf("Yhdistetään porttiin %d",routerAddress);
           } catch (UnknownHostException e) {
               System.err.println("Don't know about host: " + serverHostname);
           
           } catch (IOException e) {
               System.err.println("Couldn't get I/O for "
                                  + "the connection to: " + routerAddress);
              
           }
           
   	BufferedReader stdIn = new BufferedReader(
                                      new InputStreamReader(System.in));
   	

           System.out.print ("input: ");
           reachable = true;
           
           
   	while (on = true)  {
	
   		if(viesti.charAt(1) != '-'){
   		out.println(viesti +"->"+ RouterAddress2);
   		System.out.println("cala: " + RouterAddress2 + ": FORWARDED:" + viesti );
	   
	    try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			
			 Thread.currentThread().interrupt();
			
		}
	       
	      }
   		
   		
   		
   		else {
   			out.println(viesti);
   	   		System.out.println("cala: "+RouterAddress2 + ": FORWARDED:" + viesti );
   		  
   		    try {
   				Thread.sleep(3000);
   			} catch (InterruptedException e) {
   				
   				 Thread.currentThread().interrupt();
   				
   			}
   		}
   		
   			
	}
   	out.flush();
   	out.close();
   	try {
		in.close();
		stdIn.close();
	   	echoSocket.close();
	   	reachable = false;
	} catch (IOException e) {
		
		Thread.currentThread().interrupt();
	}
   	
       }
   }

	}
   

   
